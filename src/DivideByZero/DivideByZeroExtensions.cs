// Copyright 2023 Mikkel Kjeldsen
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
namespace DivideByZero;

/// <summary>
/// Ergonomic extension methods to safely divide two numbers, returning a specified result in case
/// the denominator is zero.
/// </summary>
public static class DivideByZeroExtensions
{
    /// <summary>
    /// Returns <paramref name="lhs"/> divided by <paramref name="rhs"/> if the latter is non-zero,
    /// or else zero.
    /// </summary>
    /// <param name="lhs">The numerator</param>
    /// <param name="rhs">The denominator</param>
    /// <returns><c>lhs / rhs</c>, or <c>0</c> if <c>rhs is 0</c>.</returns>
    public static decimal DivOrZero(this decimal lhs, decimal rhs)
    {
        return DivOrDefault(lhs, rhs, 0);
    }

    /// <summary>
    /// Returns <paramref name="lhs"/> divided by <paramref name="rhs"/> if both have values and the
    /// latter is non-zero, or else zero.
    /// </summary>
    /// <param name="lhs">The numerator</param>
    /// <param name="rhs">The denominator</param>
    /// <returns><c>lhs / rhs</c>, or <c>0</c> if either <c>lhs is null</c> or <c>rhs is null or
    /// 0</c>.</returns>
    public static decimal DivOrZero(this decimal? lhs, decimal? rhs)
    {
        return lhs.HasValue && rhs.HasValue ? lhs.Value.DivOrZero(rhs.Value) : 0;
    }

    /// <summary>
    /// Returns <paramref name="lhs"/> divided by <paramref name="rhs"/> if the latter is non-zero,
    /// or else the multiplicative identity one.
    /// </summary>
    /// <param name="lhs">The numerator</param>
    /// <param name="rhs">The denominator</param>
    /// <returns><c>lhs / rhs</c>, or <c>1</c> if <c>rhs is 0</c>.</returns>
    public static decimal DivOrOne(this decimal lhs, decimal rhs)
    {
        return DivOrDefault(lhs, rhs, 1);
    }

    /// <summary>
    /// Returns <paramref name="lhs"/> divided by <paramref name="rhs"/> if both have values and the
    /// latter is non-zero, or else the multiplicative identity one.
    /// </summary>
    /// <param name="lhs">The numerator</param>
    /// <param name="rhs">The denominator</param>
    /// <returns><c>lhs / rhs</c>, or <c>1</c> if either <c>lhs is null</c> or <c>rhs is null or
    /// 0</c>.</returns>
    public static decimal DivOrOne(this decimal? lhs, decimal? rhs)
    {
        return lhs.HasValue && rhs.HasValue ? lhs.Value.DivOrOne(rhs.Value) : 1;
    }

    /// <summary>
    /// Returns <paramref name="lhs"/> divided by <paramref name="rhs"/> if the latter is non-zero,
    /// or else <paramref name="lhs"/>.
    /// </summary>
    /// <param name="lhs">The numerator</param>
    /// <param name="rhs">The denominator</param>
    /// <returns><c>lhs / rhs</c>, or <paramref name="lhs"/> if <c>rhs is 0</c>.</returns>
    public static decimal DivOrLhs(this decimal lhs, decimal rhs)
    {
        return DivOrDefault(lhs, rhs, lhs);
    }

    /// <summary>
    /// Returns <paramref name="lhs"/> divided by <paramref name="rhs"/> if both have values and the
    /// latter is non-zero, or else <paramref name="lhs"/>.
    /// </summary>
    /// <param name="lhs">The numerator</param>
    /// <param name="rhs">The denominator</param>
    /// <returns><c>lhs / rhs</c>, or <paramref name="lhs"/> if either <c>lhs is null</c> or <c>rhs
    /// is null or 0</c>.</returns>
    public static decimal? DivOrLhs(this decimal? lhs, decimal? rhs)
    {
        return lhs.HasValue && rhs.HasValue ? lhs.Value.DivOrLhs(rhs.Value) : lhs;
    }

    /// <summary>
    /// Returns <paramref name="lhs"/> divided by <paramref name="rhs"/> if the latter is non-zero,
    /// or else <c>null</c>.
    /// </summary>
    /// <param name="lhs">The numerator</param>
    /// <param name="rhs">The denominator</param>
    /// <returns><c>lhs / rhs</c>, or <c>null</c> if <c>rhs is 0</c>.</returns>
    public static decimal? DivOrNull(this decimal lhs, decimal rhs)
    {
        return rhs is 0 ? null : lhs / rhs;
    }

    /// <summary>
    /// Returns <paramref name="lhs"/> divided by <paramref name="rhs"/> if both have values and the
    /// latter is non-zero, or else <c>null</c>.
    /// </summary>
    /// <param name="lhs">The numerator</param>
    /// <param name="rhs">The denominator</param>
    /// <returns><c>lhs / rhs</c>, or <c>null</c> if <c>rhs is 0</c>.</returns>
    public static decimal? DivOrNull(this decimal? lhs, decimal? rhs)
    {
        return rhs is 0 ? null : lhs / rhs;
    }

    /// <summary>
    /// Returns <paramref name="lhs"/> divided by <paramref name="rhs"/> if the latter is non-zero,
    /// or else zero.
    /// </summary>
    /// <param name="lhs">The numerator</param>
    /// <param name="rhs">The denominator</param>
    /// <returns><c>lhs / rhs</c>, or <c>0</c> if <c>rhs is 0</c>.</returns>
    public static double DivOrZero(this double lhs, double rhs)
    {
        return DivOrDefault(lhs, rhs, 0);
    }

    /// <summary>
    /// Returns <paramref name="lhs"/> divided by <paramref name="rhs"/> if both have values and the
    /// latter is non-zero, or else zero.
    /// </summary>
    /// <param name="lhs">The numerator</param>
    /// <param name="rhs">The denominator</param>
    /// <returns><c>lhs / rhs</c>, or <c>0</c> if either <c>lhs is null</c> or <c>rhs is null or
    /// 0</c>.</returns>
    public static double DivOrZero(this double? lhs, double? rhs)
    {
        return lhs.HasValue && rhs.HasValue ? lhs.Value.DivOrZero(rhs.Value) : 0;
    }

    /// <summary>
    /// Returns <paramref name="lhs"/> divided by <paramref name="rhs"/> if the latter is non-zero,
    /// or else the multiplicative identity one.
    /// </summary>
    /// <param name="lhs">The numerator</param>
    /// <param name="rhs">The denominator</param>
    /// <returns><c>lhs / rhs</c>, or <c>1</c> if <c>rhs is 0</c>.</returns>
    public static double DivOrOne(this double lhs, double rhs)
    {
        return DivOrDefault(lhs, rhs, 1);
    }

    /// <summary>
    /// Returns <paramref name="lhs"/> divided by <paramref name="rhs"/> if both have values and the
    /// latter is non-zero, or else the multiplicative identity one.
    /// </summary>
    /// <param name="lhs">The numerator</param>
    /// <param name="rhs">The denominator</param>
    /// <returns><c>lhs / rhs</c>, or <c>1</c> if either <c>lhs is null</c> or <c>rhs is null or
    /// 0</c>.</returns>
    public static double DivOrOne(this double? lhs, double? rhs)
    {
        return lhs.HasValue && rhs.HasValue ? lhs.Value.DivOrOne(rhs.Value) : 1;
    }

    /// <summary>
    /// Returns <paramref name="lhs"/> divided by <paramref name="rhs"/> if the latter is non-zero,
    /// or else <paramref name="lhs"/>.
    /// </summary>
    /// <param name="lhs">The numerator</param>
    /// <param name="rhs">The denominator</param>
    /// <returns><c>lhs / rhs</c>, or <paramref name="lhs"/> if <c>rhs is 0</c>.</returns>
    public static double DivOrLhs(this double lhs, double rhs)
    {
        return DivOrDefault(lhs, rhs, lhs);
    }

    /// <summary>
    /// Returns <paramref name="lhs"/> divided by <paramref name="rhs"/> if both have values and the
    /// latter is non-zero, or else <paramref name="lhs"/>.
    /// </summary>
    /// <param name="lhs">The numerator</param>
    /// <param name="rhs">The denominator</param>
    /// <returns><c>lhs / rhs</c>, or <paramref name="lhs"/> if either <c>lhs is null</c> or <c>rhs
    /// is null or 0</c>.</returns>
    public static double? DivOrLhs(this double? lhs, double? rhs)
    {
        return lhs.HasValue && rhs.HasValue ? lhs.Value.DivOrLhs(rhs.Value) : lhs;
    }

    /// <summary>
    /// Returns <paramref name="lhs"/> divided by <paramref name="rhs"/> if the latter is non-zero,
    /// or else <c>null</c>.
    /// </summary>
    /// <param name="lhs">The numerator</param>
    /// <param name="rhs">The denominator</param>
    /// <returns><c>lhs / rhs</c>, or <c>null</c> if <c>rhs is 0</c>.</returns>
    public static double? DivOrNull(this double lhs, double rhs)
    {
        return rhs is 0 ? null : lhs / rhs;
    }

    /// <summary>
    /// Returns <paramref name="lhs"/> divided by <paramref name="rhs"/> if both have values and the
    /// latter is non-zero, or else <c>null</c>.
    /// </summary>
    /// <param name="lhs">The numerator</param>
    /// <param name="rhs">The denominator</param>
    /// <returns><c>lhs / rhs</c>, or <c>null</c> if <c>rhs is 0</c>.</returns>
    public static double? DivOrNull(this double? lhs, double? rhs)
    {
        return rhs is 0 ? null : lhs / rhs;
    }

    /// <summary>
    /// Returns <paramref name="lhs"/> divided by <paramref name="rhs"/> if the latter is non-zero,
    /// or else zero.
    /// </summary>
    /// <param name="lhs">The numerator</param>
    /// <param name="rhs">The denominator</param>
    /// <returns><c>lhs / rhs</c>, or <c>0</c> if <c>rhs is 0</c>.</returns>
    public static float DivOrZero(this float lhs, float rhs)
    {
        return DivOrDefault(lhs, rhs, 0);
    }

    /// <summary>
    /// Returns <paramref name="lhs"/> divided by <paramref name="rhs"/> if both have values and the
    /// latter is non-zero, or else zero.
    /// </summary>
    /// <param name="lhs">The numerator</param>
    /// <param name="rhs">The denominator</param>
    /// <returns><c>lhs / rhs</c>, or <c>0</c> if either <c>lhs is null</c> or <c>rhs is null or
    /// 0</c>.</returns>
    public static float DivOrZero(this float? lhs, float? rhs)
    {
        return lhs.HasValue && rhs.HasValue ? lhs.Value.DivOrZero(rhs.Value) : 0;
    }

    /// <summary>
    /// Returns <paramref name="lhs"/> divided by <paramref name="rhs"/> if the latter is non-zero,
    /// or else the multiplicative identity one.
    /// </summary>
    /// <param name="lhs">The numerator</param>
    /// <param name="rhs">The denominator</param>
    /// <returns><c>lhs / rhs</c>, or <c>1</c> if <c>rhs is 0</c>.</returns>
    public static float DivOrOne(this float lhs, float rhs)
    {
        return DivOrDefault(lhs, rhs, 1);
    }

    /// <summary>
    /// Returns <paramref name="lhs"/> divided by <paramref name="rhs"/> if both have values and the
    /// latter is non-zero, or else the multiplicative identity one.
    /// </summary>
    /// <param name="lhs">The numerator</param>
    /// <param name="rhs">The denominator</param>
    /// <returns><c>lhs / rhs</c>, or <c>1</c> if either <c>lhs is null</c> or <c>rhs is null or
    /// 0</c>.</returns>
    public static float DivOrOne(this float? lhs, float? rhs)
    {
        return lhs.HasValue && rhs.HasValue ? lhs.Value.DivOrOne(rhs.Value) : 1;
    }

    /// <summary>
    /// Returns <paramref name="lhs"/> divided by <paramref name="rhs"/> if the latter is non-zero,
    /// or else <paramref name="lhs"/>.
    /// </summary>
    /// <param name="lhs">The numerator</param>
    /// <param name="rhs">The denominator</param>
    /// <returns><c>lhs / rhs</c>, or <paramref name="lhs"/> if <c>rhs is 0</c>.</returns>
    public static float DivOrLhs(this float lhs, float rhs)
    {
        return DivOrDefault(lhs, rhs, lhs);
    }

    /// <summary>
    /// Returns <paramref name="lhs"/> divided by <paramref name="rhs"/> if both have values and the
    /// latter is non-zero, or else <paramref name="lhs"/>.
    /// </summary>
    /// <param name="lhs">The numerator</param>
    /// <param name="rhs">The denominator</param>
    /// <returns><c>lhs / rhs</c>, or <paramref name="lhs"/> if either <c>lhs is null</c> or <c>rhs
    /// is null or 0</c>.</returns>
    public static float? DivOrLhs(this float? lhs, float? rhs)
    {
        return lhs.HasValue && rhs.HasValue ? lhs.Value.DivOrLhs(rhs.Value) : lhs;
    }

    /// <summary>
    /// Returns <paramref name="lhs"/> divided by <paramref name="rhs"/> if the latter is non-zero,
    /// or else <c>null</c>.
    /// </summary>
    /// <param name="lhs">The numerator</param>
    /// <param name="rhs">The denominator</param>
    /// <returns><c>lhs / rhs</c>, or <c>null</c> if <c>rhs is 0</c>.</returns>
    public static float? DivOrNull(this float lhs, float rhs)
    {
        return rhs is 0 ? null : lhs / rhs;
    }

    /// <summary>
    /// Returns <paramref name="lhs"/> divided by <paramref name="rhs"/> if both have values and the
    /// latter is non-zero, or else <c>null</c>.
    /// </summary>
    /// <param name="lhs">The numerator</param>
    /// <param name="rhs">The denominator</param>
    /// <returns><c>lhs / rhs</c>, or <c>null</c> if <c>rhs is 0</c>.</returns>
    public static float? DivOrNull(this float? lhs, float? rhs)
    {
        return rhs is 0 ? null : lhs / rhs;
    }

    /// <summary>
    /// Returns <paramref name="lhs"/> divided by <paramref name="rhs"/> if the latter is non-zero,
    /// or else zero.
    /// </summary>
    /// <param name="lhs">The numerator</param>
    /// <param name="rhs">The denominator</param>
    /// <returns><c>lhs / rhs</c>, or <c>0</c> if <c>rhs is 0</c>.</returns>
    public static long DivOrZero(this long lhs, long rhs)
    {
        return DivOrDefault(lhs, rhs, 0);
    }

    /// <summary>
    /// Returns <paramref name="lhs"/> divided by <paramref name="rhs"/> if both have values and the
    /// latter is non-zero, or else zero.
    /// </summary>
    /// <param name="lhs">The numerator</param>
    /// <param name="rhs">The denominator</param>
    /// <returns><c>lhs / rhs</c>, or <c>0</c> if either <c>lhs is null</c> or <c>rhs is null or
    /// 0</c>.</returns>
    public static long DivOrZero(this long? lhs, long? rhs)
    {
        return lhs.HasValue && rhs.HasValue ? lhs.Value.DivOrZero(rhs.Value) : 0;
    }

    /// <summary>
    /// Returns <paramref name="lhs"/> divided by <paramref name="rhs"/> if the latter is non-zero,
    /// or else the multiplicative identity one.
    /// </summary>
    /// <param name="lhs">The numerator</param>
    /// <param name="rhs">The denominator</param>
    /// <returns><c>lhs / rhs</c>, or <c>1</c> if <c>rhs is 0</c>.</returns>
    public static long DivOrOne(this long lhs, long rhs)
    {
        return DivOrDefault(lhs, rhs, 1);
    }

    /// <summary>
    /// Returns <paramref name="lhs"/> divided by <paramref name="rhs"/> if both have values and the
    /// latter is non-zero, or else the multiplicative identity one.
    /// </summary>
    /// <param name="lhs">The numerator</param>
    /// <param name="rhs">The denominator</param>
    /// <returns><c>lhs / rhs</c>, or <c>1</c> if either <c>lhs is null</c> or <c>rhs is null or
    /// 0</c>.</returns>
    public static long DivOrOne(this long? lhs, long? rhs)
    {
        return lhs.HasValue && rhs.HasValue ? lhs.Value.DivOrOne(rhs.Value) : 1;
    }

    /// <summary>
    /// Returns <paramref name="lhs"/> divided by <paramref name="rhs"/> if the latter is non-zero,
    /// or else <paramref name="lhs"/>.
    /// </summary>
    /// <param name="lhs">The numerator</param>
    /// <param name="rhs">The denominator</param>
    /// <returns><c>lhs / rhs</c>, or <paramref name="lhs"/> if <c>rhs is 0</c>.</returns>
    public static long DivOrLhs(this long lhs, long rhs)
    {
        return DivOrDefault(lhs, rhs, lhs);
    }

    /// <summary>
    /// Returns <paramref name="lhs"/> divided by <paramref name="rhs"/> if both have values and the
    /// latter is non-zero, or else <paramref name="lhs"/>.
    /// </summary>
    /// <param name="lhs">The numerator</param>
    /// <param name="rhs">The denominator</param>
    /// <returns><c>lhs / rhs</c>, or <paramref name="lhs"/> if either <c>lhs is null</c> or <c>rhs
    /// is null or 0</c>.</returns>
    public static long? DivOrLhs(this long? lhs, long? rhs)
    {
        return lhs.HasValue && rhs.HasValue ? lhs.Value.DivOrLhs(rhs.Value) : lhs;
    }

    /// <summary>
    /// Returns <paramref name="lhs"/> divided by <paramref name="rhs"/> if the latter is non-zero,
    /// or else <c>null</c>.
    /// </summary>
    /// <param name="lhs">The numerator</param>
    /// <param name="rhs">The denominator</param>
    /// <returns><c>lhs / rhs</c>, or <c>null</c> if <c>rhs is 0</c>.</returns>
    public static long? DivOrNull(this long lhs, long rhs)
    {
        return rhs is 0 ? null : lhs / rhs;
    }

    /// <summary>
    /// Returns <paramref name="lhs"/> divided by <paramref name="rhs"/> if both have values and the
    /// latter is non-zero, or else <c>null</c>.
    /// </summary>
    /// <param name="lhs">The numerator</param>
    /// <param name="rhs">The denominator</param>
    /// <returns><c>lhs / rhs</c>, or <c>null</c> if <c>rhs is 0</c>.</returns>
    public static long? DivOrNull(this long? lhs, long? rhs)
    {
        return rhs is 0 ? null : lhs / rhs;
    }

    /// <summary>
    /// Returns <paramref name="lhs"/> divided by <paramref name="rhs"/> if the latter is non-zero,
    /// or else zero.
    /// </summary>
    /// <param name="lhs">The numerator</param>
    /// <param name="rhs">The denominator</param>
    /// <returns><c>lhs / rhs</c>, or <c>0</c> if <c>rhs is 0</c>.</returns>
    public static int DivOrZero(this int lhs, int rhs)
    {
        return DivOrDefault(lhs, rhs, 0);
    }

    /// <summary>
    /// Returns <paramref name="lhs"/> divided by <paramref name="rhs"/> if both have values and the
    /// latter is non-zero, or else zero.
    /// </summary>
    /// <param name="lhs">The numerator</param>
    /// <param name="rhs">The denominator</param>
    /// <returns><c>lhs / rhs</c>, or <c>0</c> if either <c>lhs is null</c> or <c>rhs is null or
    /// 0</c>.</returns>
    public static int DivOrZero(this int? lhs, int? rhs)
    {
        return lhs.HasValue && rhs.HasValue ? lhs.Value.DivOrZero(rhs.Value) : 0;
    }

    /// <summary>
    /// Returns <paramref name="lhs"/> divided by <paramref name="rhs"/> if the latter is non-zero,
    /// or else the multiplicative identity one.
    /// </summary>
    /// <param name="lhs">The numerator</param>
    /// <param name="rhs">The denominator</param>
    /// <returns><c>lhs / rhs</c>, or <c>1</c> if <c>rhs is 0</c>.</returns>
    public static int DivOrOne(this int lhs, int rhs)
    {
        return DivOrDefault(lhs, rhs, 1);
    }

    /// <summary>
    /// Returns <paramref name="lhs"/> divided by <paramref name="rhs"/> if both have values and the
    /// latter is non-zero, or else the multiplicative identity one.
    /// </summary>
    /// <param name="lhs">The numerator</param>
    /// <param name="rhs">The denominator</param>
    /// <returns><c>lhs / rhs</c>, or <c>1</c> if either <c>lhs is null</c> or <c>rhs is null or
    /// 0</c>.</returns>
    public static int DivOrOne(this int? lhs, int? rhs)
    {
        return lhs.HasValue && rhs.HasValue ? lhs.Value.DivOrOne(rhs.Value) : 1;
    }

    /// <summary>
    /// Returns <paramref name="lhs"/> divided by <paramref name="rhs"/> if the latter is non-zero,
    /// or else <paramref name="lhs"/>.
    /// </summary>
    /// <param name="lhs">The numerator</param>
    /// <param name="rhs">The denominator</param>
    /// <returns><c>lhs / rhs</c>, or <paramref name="lhs"/> if <c>rhs is 0</c>.</returns>
    public static int DivOrLhs(this int lhs, int rhs)
    {
        return DivOrDefault(lhs, rhs, lhs);
    }

    /// <summary>
    /// Returns <paramref name="lhs"/> divided by <paramref name="rhs"/> if both have values and the
    /// latter is non-zero, or else <paramref name="lhs"/>.
    /// </summary>
    /// <param name="lhs">The numerator</param>
    /// <param name="rhs">The denominator</param>
    /// <returns><c>lhs / rhs</c>, or <paramref name="lhs"/> if either <c>lhs is null</c> or <c>rhs
    /// is null or 0</c>.</returns>
    public static int? DivOrLhs(this int? lhs, int? rhs)
    {
        return lhs.HasValue && rhs.HasValue ? lhs.Value.DivOrLhs(rhs.Value) : lhs;
    }

    /// <summary>
    /// Returns <paramref name="lhs"/> divided by <paramref name="rhs"/> if the latter is non-zero,
    /// or else <c>null</c>.
    /// </summary>
    /// <param name="lhs">The numerator</param>
    /// <param name="rhs">The denominator</param>
    /// <returns><c>lhs / rhs</c>, or <c>null</c> if <c>rhs is 0</c>.</returns>
    public static int? DivOrNull(this int lhs, int rhs)
    {
        return rhs is 0 ? null : lhs / rhs;
    }

    /// <summary>
    /// Returns <paramref name="lhs"/> divided by <paramref name="rhs"/> if both have values and the
    /// latter is non-zero, or else <c>null</c>.
    /// </summary>
    /// <param name="lhs">The numerator</param>
    /// <param name="rhs">The denominator</param>
    /// <returns><c>lhs / rhs</c>, or <c>null</c> if <c>rhs is 0</c>.</returns>
    public static int? DivOrNull(this int? lhs, int? rhs)
    {
        return rhs is 0 ? null : lhs / rhs;
    }

    private static decimal DivOrDefault(decimal lhs, decimal rhs, decimal def)
    {
        return rhs is 0 ? def : lhs / rhs;
    }

    private static double DivOrDefault(double lhs, double rhs, double def)
    {
        return rhs is 0 ? def : lhs / rhs;
    }

    private static float DivOrDefault(float lhs, float rhs, float def)
    {
        return rhs is 0 ? def : lhs / rhs;
    }

    private static long DivOrDefault(long lhs, long rhs, long def)
    {
        return rhs is 0 ? def : lhs / rhs;
    }

    private static int DivOrDefault(int lhs, int rhs, int def)
    {
        return rhs is 0 ? def : lhs / rhs;
    }
}
