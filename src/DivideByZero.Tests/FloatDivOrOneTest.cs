using DivideByZero;
using System;
using Xunit;

namespace DivideByZero.Tests;

public class FloatDivOrOneTest : AbstractContractTest<float>
{
    [Fact]
    public override void ApiNonNullable()
    {
        float lhs = (rng.Next() | 1);
        float rhs = rng.Next();
        var expected = lhs / rhs;
        Assert.Equal(expected, lhs.DivOrOne(rhs));
    }

    [Fact]
    public override void ApiNullableLhs()
    {
        float? lhs = null;
        float? rhs = rng.Next();
        Assert.Equal(1, lhs.DivOrOne(rhs));
    }

    [Fact]
    public override void ApiNullableRhs()
    {
        float? lhs = rng.Next();
        float? rhs = null;
        Assert.Equal(1, lhs.DivOrOne(rhs));
    }

    [Fact]
    public override void ApiNullableLhsRhs()
    {
        float? lhs = null;
        float? rhs = null;
        Assert.Equal(1, lhs.DivOrOne(rhs));
    }

    [Theory]
    [ClassData(typeof(DivOrOneData))]
    public override void CorrectnessReference(float lhs, float rhs, float? expected)
    {
        float actual = lhs / rhs;
        if (IsDivByZeroLike(actual))
        {
            actual = 1;
        }
        Assert.Equal(expected, actual);
    }

    [Theory]
    [ClassData(typeof(DivOrOneData))]
    public override void CorrectnessNonNullable(float lhs, float rhs, float? expected)
    {
        Assert.Equal(expected, lhs.DivOrOne(rhs));
    }

    [Theory]
    [ClassData(typeof(DivOrOneData))]
    public override void CorrectnessNullable(float? lhs, float? rhs, float? expected)
    {
        Assert.Equal(expected, lhs.DivOrOne(rhs));
    }

    private class DivOrOneData : ContractTestData.NonNullable<float>
    {
        public DivOrOneData()
        {
            Add(-1, -1, 1);
            Add(-1, 1, -1);
            Add(0, 1, 0);
            Add(1, -1, -1);
            Add(1, 1, 1);
            Add(4, 2, 2);

            Add(-1, 0, 1);
            Add(0, 0, 1);
            Add(1, 0, 1);
        }
    }
}
