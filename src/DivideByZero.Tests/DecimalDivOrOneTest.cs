using DivideByZero;
using System;
using Xunit;

namespace DivideByZero.Tests;

public class DecimalDivOrOneTest : AbstractContractTest<decimal>
{
    [Fact]
    public override void ApiNonNullable()
    {
        decimal lhs = (rng.Next() | 1);
        decimal rhs = rng.Next();
        var expected = lhs / rhs;
        Assert.Equal(expected, lhs.DivOrOne(rhs));
    }

    [Fact]
    public override void ApiNullableLhs()
    {
        decimal? lhs = null;
        decimal? rhs = rng.Next();
        Assert.Equal(1, lhs.DivOrOne(rhs));
    }

    [Fact]
    public override void ApiNullableRhs()
    {
        decimal? lhs = rng.Next();
        decimal? rhs = null;
        Assert.Equal(1, lhs.DivOrOne(rhs));
    }

    [Fact]
    public override void ApiNullableLhsRhs()
    {
        decimal? lhs = null;
        decimal? rhs = null;
        Assert.Equal(1, lhs.DivOrOne(rhs));
    }

    [Theory]
    [ClassData(typeof(DivOrOneData))]
    public override void CorrectnessReference(decimal lhs, decimal rhs, decimal? expected)
    {
        decimal actual;
        try
        {
            actual = lhs / rhs;
        }
        catch (DivideByZeroException)
        {
            actual = 1;
        }
        Assert.Equal(expected, actual);
    }

    [Theory]
    [ClassData(typeof(DivOrOneData))]
    public override void CorrectnessNonNullable(decimal lhs, decimal rhs, decimal? expected)
    {
        Assert.Equal(expected, lhs.DivOrOne(rhs));
    }

    [Theory]
    [ClassData(typeof(DivOrOneData))]
    public override void CorrectnessNullable(decimal? lhs, decimal? rhs, decimal? expected)
    {
        Assert.Equal(expected, lhs.DivOrOne(rhs));
    }

    private class DivOrOneData : ContractTestData.NonNullable<decimal>
    {
        public DivOrOneData()
        {
            Add(-1, -1, 1);
            Add(-1, 1, -1);
            Add(0, 1, 0);
            Add(1, -1, -1);
            Add(1, 1, 1);
            Add(4, 2, 2);

            Add(-1, 0, 1);
            Add(0, 0, 1);
            Add(1, 0, 1);
        }
    }
}
