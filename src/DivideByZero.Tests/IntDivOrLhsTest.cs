using DivideByZero;
using System;
using Xunit;

namespace DivideByZero.Tests;

public class IntDivOrLhsTest : AbstractContractTest<int>
{
    [Fact]
    public override void ApiNonNullable()
    {
        int lhs = (rng.Next() | 1);
        int rhs = rng.Next();
        var expected = lhs / rhs;
        Assert.Equal(expected, lhs.DivOrLhs(rhs));
    }

    [Fact]
    public override void ApiNullableLhs()
    {
        int? lhs = null;
        int? rhs = rng.Next();
        Assert.Equal(lhs, lhs.DivOrLhs(rhs));
    }

    [Fact]
    public override void ApiNullableRhs()
    {
        int? lhs = rng.Next();
        int? rhs = null;
        Assert.Equal(lhs, lhs.DivOrLhs(rhs));
    }

    [Fact]
    public override void ApiNullableLhsRhs()
    {
        int? lhs = null;
        int? rhs = null;
        Assert.Equal(lhs, lhs.DivOrLhs(rhs));
    }

    [Theory]
    [ClassData(typeof(DivOrLhsData))]
    public override void CorrectnessReference(int lhs, int rhs, int? expected)
    {
        int actual;
        try
        {
            actual = lhs / rhs;
        }
        catch (DivideByZeroException)
        {
            actual = lhs;
        }
        Assert.Equal(expected, actual);
    }

    [Theory]
    [ClassData(typeof(DivOrLhsData))]
    public override void CorrectnessNonNullable(int lhs, int rhs, int? expected)
    {
        Assert.Equal(expected, lhs.DivOrLhs(rhs));
    }

    [Theory]
    [ClassData(typeof(DivOrLhsData))]
    public override void CorrectnessNullable(int? lhs, int? rhs, int? expected)
    {
        Assert.Equal(expected, lhs.DivOrLhs(rhs));
    }

    private class DivOrLhsData : ContractTestData.NonNullable<int>
    {
        public DivOrLhsData()
        {
            Add(-1, -1, 1);
            Add(-1, 1, -1);
            Add(0, 1, 0);
            Add(1, -1, -1);
            Add(1, 1, 1);
            Add(4, 2, 2);

            Add(-1, 0, -1);
            Add(0, 0, 0);
            Add(1, 0, 1);
        }
    }
}
