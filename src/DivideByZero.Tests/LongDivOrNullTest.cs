using DivideByZero;
using System;
using Xunit;

namespace DivideByZero.Tests;

public class LongDivOrNullTest : AbstractContractTest<long>
{
    [Fact]
    public override void ApiNonNullable()
    {
        long lhs = (rng.Next() | 1);
        long rhs = rng.Next();
        var expected = lhs / rhs;
        Assert.Equal(expected, lhs.DivOrNull(rhs));
    }

    [Fact]
    public override void ApiNullableLhs()
    {
        long? lhs = null;
        long? rhs = rng.Next();
        Assert.Null(lhs.DivOrNull(rhs));
    }

    [Fact]
    public override void ApiNullableRhs()
    {
        long? lhs = rng.Next();
        long? rhs = null;
        Assert.Null(lhs.DivOrNull(rhs));
    }

    [Fact]
    public override void ApiNullableLhsRhs()
    {
        long? lhs = null;
        long? rhs = null;
        Assert.Null(lhs.DivOrNull(rhs));
    }

    [Theory]
    [ClassData(typeof(DivOrNullData))]
    public override void CorrectnessReference(long lhs, long rhs, long? expected)
    {
        long? actual;
        try
        {
            actual = lhs / rhs;
        }
        catch (DivideByZeroException)
        {
            actual = null;
        }
        Assert.Equal(expected, actual);
    }

    [Theory]
    [ClassData(typeof(DivOrNullData))]
    public override void CorrectnessNonNullable(long lhs, long rhs, long? expected)
    {
        Assert.Equal(expected, lhs.DivOrNull(rhs));
    }

    [Theory]
    [ClassData(typeof(DivOrNullData))]
    public override void CorrectnessNullable(long? lhs, long? rhs, long? expected)
    {
        Assert.Equal(expected, lhs.DivOrNull(rhs));
    }

    private class DivOrNullData : ContractTestData.Nullable<long>
    {
        public DivOrNullData()
        {
            Add(-1, -1, 1);
            Add(-1, 1, -1);
            Add(0, 1, 0);
            Add(1, -1, -1);
            Add(1, 1, 1);
            Add(4, 2, 2);

            Add(-1, 0, null);
            Add(0, 0, null);
            Add(1, 0, null);
        }
    }
}
