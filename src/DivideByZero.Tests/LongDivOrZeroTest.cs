using DivideByZero;
using System;
using Xunit;

namespace DivideByZero.Tests;

public class LongDivOrZeroTest : AbstractContractTest<long>
{
    [Fact]
    public override void ApiNonNullable()
    {
        long lhs = (rng.Next() | 1);
        long rhs = rng.Next();
        var expected = lhs / rhs;
        Assert.Equal(expected, lhs.DivOrZero(rhs));
    }

    [Fact]
    public override void ApiNullableLhs()
    {
        long? lhs = null;
        long? rhs = rng.Next();
        Assert.Equal(0, lhs.DivOrZero(rhs));
    }

    [Fact]
    public override void ApiNullableRhs()
    {
        long? lhs = rng.Next();
        long? rhs = null;
        Assert.Equal(0, lhs.DivOrZero(rhs));
    }

    [Fact]
    public override void ApiNullableLhsRhs()
    {
        long? lhs = null;
        long? rhs = null;
        Assert.Equal(0, lhs.DivOrZero(rhs));
    }

    [Theory]
    [ClassData(typeof(DivOrZeroData))]
    public override void CorrectnessReference(long lhs, long rhs, long? expected)
    {
        long actual;
        try
        {
            actual = lhs / rhs;
        }
        catch (DivideByZeroException)
        {
            actual = 0;
        }
        Assert.Equal(expected, actual);
    }

    [Theory]
    [ClassData(typeof(DivOrZeroData))]
    public override void CorrectnessNonNullable(long lhs, long rhs, long? expected)
    {
        Assert.Equal(expected, lhs.DivOrZero(rhs));
    }

    [Theory]
    [ClassData(typeof(DivOrZeroData))]
    public override void CorrectnessNullable(long? lhs, long? rhs, long? expected)
    {
        Assert.Equal(expected, lhs.DivOrZero(rhs));
    }

    private class DivOrZeroData : ContractTestData.NonNullable<long>
    {
        public DivOrZeroData()
        {
            Add(-1, -1, 1);
            Add(-1, 1, -1);
            Add(0, 1, 0);
            Add(1, -1, -1);
            Add(1, 1, 1);
            Add(4, 2, 2);

            Add(-1, 0, 0);
            Add(0, 0, 0);
            Add(1, 0, 0);
        }
    }
}
