using System;

public abstract class AbstractContractTest<T>
    where T : struct
{
    protected readonly Random rng = new Random();

    public abstract void ApiNonNullable();
    public abstract void ApiNullableLhs();
    public abstract void ApiNullableLhsRhs();
    public abstract void ApiNullableRhs();
    public abstract void CorrectnessReference(T lhs, T rhs, T? expected);
    public abstract void CorrectnessNonNullable(T lhs, T rhs, T? expected);
    public abstract void CorrectnessNullable(T? lhs, T? rhs, T? expected);

    protected static bool IsDivByZeroLike(double d)
    {
        return double.IsNaN(d) || double.IsInfinity(d) || double.IsNegativeInfinity(d);
    }

    protected static bool IsDivByZeroLike(float f)
    {
        return IsDivByZeroLike((double)f);
    }
}
