using DivideByZero;
using System;
using Xunit;

namespace DivideByZero.Tests;

public class FloatDivOrLhsTest : AbstractContractTest<float>
{
    [Fact]
    public override void ApiNonNullable()
    {
        float lhs = (rng.Next() | 1);
        float rhs = rng.Next();
        var expected = lhs / rhs;
        Assert.Equal(expected, lhs.DivOrLhs(rhs));
    }

    [Fact]
    public override void ApiNullableLhs()
    {
        float? lhs = null;
        float? rhs = rng.Next();
        Assert.Equal(lhs, lhs.DivOrLhs(rhs));
    }

    [Fact]
    public override void ApiNullableRhs()
    {
        float? lhs = rng.Next();
        float? rhs = null;
        Assert.Equal(lhs, lhs.DivOrLhs(rhs));
    }

    [Fact]
    public override void ApiNullableLhsRhs()
    {
        float? lhs = null;
        float? rhs = null;
        Assert.Equal(lhs, lhs.DivOrLhs(rhs));
    }

    [Theory]
    [ClassData(typeof(DivOrLhsData))]
    public override void CorrectnessReference(float lhs, float rhs, float? expected)
    {
        float actual = lhs / rhs;
        if (IsDivByZeroLike(actual))
        {
            actual = lhs;
        }
        Assert.Equal(expected, actual);
    }

    [Theory]
    [ClassData(typeof(DivOrLhsData))]
    public override void CorrectnessNonNullable(float lhs, float rhs, float? expected)
    {
        Assert.Equal(expected, lhs.DivOrLhs(rhs));
    }

    [Theory]
    [ClassData(typeof(DivOrLhsData))]
    public override void CorrectnessNullable(float? lhs, float? rhs, float? expected)
    {
        Assert.Equal(expected, lhs.DivOrLhs(rhs));
    }

    private class DivOrLhsData : ContractTestData.NonNullable<float>
    {
        public DivOrLhsData()
        {
            Add(-1, -1, 1);
            Add(-1, 1, -1);
            Add(0, 1, 0);
            Add(1, -1, -1);
            Add(1, 1, 1);
            Add(4, 2, 2);

            Add(-1, 0, -1);
            Add(0, 0, 0);
            Add(1, 0, 1);
        }
    }
}
