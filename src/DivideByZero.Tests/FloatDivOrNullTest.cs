using DivideByZero;
using System;
using Xunit;

namespace DivideByZero.Tests;

public class FloatDivOrNullTest : AbstractContractTest<float>
{
    [Fact]
    public override void ApiNonNullable()
    {
        float lhs = (rng.Next() | 1);
        float rhs = rng.Next();
        var expected = lhs / rhs;
        Assert.Equal(expected, lhs.DivOrNull(rhs));
    }

    [Fact]
    public override void ApiNullableLhs()
    {
        float? lhs = null;
        float? rhs = rng.Next();
        Assert.Null(lhs.DivOrNull(rhs));
    }

    [Fact]
    public override void ApiNullableRhs()
    {
        float? lhs = rng.Next();
        float? rhs = null;
        Assert.Null(lhs.DivOrNull(rhs));
    }

    [Fact]
    public override void ApiNullableLhsRhs()
    {
        float? lhs = null;
        float? rhs = null;
        Assert.Null(lhs.DivOrNull(rhs));
    }

    [Theory]
    [ClassData(typeof(DivOrNullData))]
    public override void CorrectnessReference(float lhs, float rhs, float? expected)
    {
        float? actual = lhs / rhs;
        if (IsDivByZeroLike(actual.Value))
        {
            actual = null;
        }
        Assert.Equal(expected, actual);
    }

    [Theory]
    [ClassData(typeof(DivOrNullData))]
    public override void CorrectnessNonNullable(float lhs, float rhs, float? expected)
    {
        Assert.Equal(expected, lhs.DivOrNull(rhs));
    }

    [Theory]
    [ClassData(typeof(DivOrNullData))]
    public override void CorrectnessNullable(float? lhs, float? rhs, float? expected)
    {
        Assert.Equal(expected, lhs.DivOrNull(rhs));
    }

    private class DivOrNullData : ContractTestData.Nullable<float>
    {
        public DivOrNullData()
        {
            Add(-1, -1, 1);
            Add(-1, 1, -1);
            Add(0, 1, 0);
            Add(1, -1, -1);
            Add(1, 1, 1);
            Add(4, 2, 2);

            Add(-1, 0, null);
            Add(0, 0, null);
            Add(1, 0, null);
        }
    }
}
