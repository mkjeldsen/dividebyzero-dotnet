using DivideByZero;
using System;
using Xunit;

namespace DivideByZero.Tests;

public class DecimalDivOrLhsTest : AbstractContractTest<decimal>
{
    [Fact]
    public override void ApiNonNullable()
    {
        decimal lhs = (rng.Next() | 1);
        decimal rhs = rng.Next();
        var expected = lhs / rhs;
        Assert.Equal(expected, lhs.DivOrLhs(rhs));
    }

    [Fact]
    public override void ApiNullableLhs()
    {
        decimal? lhs = null;
        decimal? rhs = rng.Next();
        Assert.Equal(lhs, lhs.DivOrLhs(rhs));
    }

    [Fact]
    public override void ApiNullableRhs()
    {
        decimal? lhs = rng.Next();
        decimal? rhs = null;
        Assert.Equal(lhs, lhs.DivOrLhs(rhs));
    }

    [Fact]
    public override void ApiNullableLhsRhs()
    {
        decimal? lhs = null;
        decimal? rhs = null;
        Assert.Equal(lhs, lhs.DivOrLhs(rhs));
    }

    [Theory]
    [ClassData(typeof(DivOrLhsData))]
    public override void CorrectnessReference(decimal lhs, decimal rhs, decimal? expected)
    {
        decimal actual;
        try
        {
            actual = lhs / rhs;
        }
        catch (DivideByZeroException)
        {
            actual = lhs;
        }
        Assert.Equal(expected, actual);
    }

    [Theory]
    [ClassData(typeof(DivOrLhsData))]
    public override void CorrectnessNonNullable(decimal lhs, decimal rhs, decimal? expected)
    {
        Assert.Equal(expected, lhs.DivOrLhs(rhs));
    }

    [Theory]
    [ClassData(typeof(DivOrLhsData))]
    public override void CorrectnessNullable(decimal? lhs, decimal? rhs, decimal? expected)
    {
        Assert.Equal(expected, lhs.DivOrLhs(rhs));
    }

    private class DivOrLhsData : ContractTestData.NonNullable<decimal>
    {
        public DivOrLhsData()
        {
            Add(-1, -1, 1);
            Add(-1, 1, -1);
            Add(0, 1, 0);
            Add(1, -1, -1);
            Add(1, 1, 1);
            Add(4, 2, 2);

            Add(-1, 0, -1);
            Add(0, 0, 0);
            Add(1, 0, 1);
        }
    }
}
