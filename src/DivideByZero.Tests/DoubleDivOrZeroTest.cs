using DivideByZero;
using System;
using Xunit;

namespace DivideByZero.Tests;

public class DoubleDivOrZeroTest : AbstractContractTest<double>
{
    [Fact]
    public override void ApiNonNullable()
    {
        double lhs = (rng.Next() | 1);
        double rhs = rng.Next();
        var expected = lhs / rhs;
        Assert.Equal(expected, lhs.DivOrZero(rhs));
    }

    [Fact]
    public override void ApiNullableLhs()
    {
        double? lhs = null;
        double? rhs = rng.Next();
        Assert.Equal(0, lhs.DivOrZero(rhs));
    }

    [Fact]
    public override void ApiNullableRhs()
    {
        double? lhs = rng.Next();
        double? rhs = null;
        Assert.Equal(0, lhs.DivOrZero(rhs));
    }

    [Fact]
    public override void ApiNullableLhsRhs()
    {
        double? lhs = null;
        double? rhs = null;
        Assert.Equal(0, lhs.DivOrZero(rhs));
    }

    [Theory]
    [ClassData(typeof(DivOrZeroData))]
    public override void CorrectnessReference(double lhs, double rhs, double? expected)
    {
        double actual = lhs / rhs;
        if (IsDivByZeroLike(actual))
        {
            actual = 0;
        }
        Assert.Equal(expected, actual);
    }

    [Theory]
    [ClassData(typeof(DivOrZeroData))]
    public override void CorrectnessNonNullable(double lhs, double rhs, double? expected)
    {
        Assert.Equal(expected, lhs.DivOrZero(rhs));
    }

    [Theory]
    [ClassData(typeof(DivOrZeroData))]
    public override void CorrectnessNullable(double? lhs, double? rhs, double? expected)
    {
        Assert.Equal(expected, lhs.DivOrZero(rhs));
    }

    private class DivOrZeroData : ContractTestData.NonNullable<double>
    {
        public DivOrZeroData()
        {
            Add(-1, -1, 1);
            Add(-1, 1, -1);
            Add(0, 1, 0);
            Add(1, -1, -1);
            Add(1, 1, 1);
            Add(4, 2, 2);

            Add(-1, 0, 0);
            Add(0, 0, 0);
            Add(1, 0, 0);
        }
    }
}
