using DivideByZero;
using System;
using Xunit;

namespace DivideByZero.Tests;

public class IntDivOrNullTest : AbstractContractTest<int>
{
    [Fact]
    public override void ApiNonNullable()
    {
        int lhs = (rng.Next() | 1);
        int rhs = rng.Next();
        var expected = lhs / rhs;
        Assert.Equal(expected, lhs.DivOrNull(rhs));
    }

    [Fact]
    public override void ApiNullableLhs()
    {
        int? lhs = null;
        int? rhs = rng.Next();
        Assert.Null(lhs.DivOrNull(rhs));
    }

    [Fact]
    public override void ApiNullableRhs()
    {
        int? lhs = rng.Next();
        int? rhs = null;
        Assert.Null(lhs.DivOrNull(rhs));
    }

    [Fact]
    public override void ApiNullableLhsRhs()
    {
        int? lhs = null;
        int? rhs = null;
        Assert.Null(lhs.DivOrNull(rhs));
    }

    [Theory]
    [ClassData(typeof(DivOrNullData))]
    public override void CorrectnessReference(int lhs, int rhs, int? expected)
    {
        int? actual;
        try
        {
            actual = lhs / rhs;
        }
        catch (DivideByZeroException)
        {
            actual = null;
        }
        Assert.Equal(expected, actual);
    }

    [Theory]
    [ClassData(typeof(DivOrNullData))]
    public override void CorrectnessNonNullable(int lhs, int rhs, int? expected)
    {
        Assert.Equal(expected, lhs.DivOrNull(rhs));
    }

    [Theory]
    [ClassData(typeof(DivOrNullData))]
    public override void CorrectnessNullable(int? lhs, int? rhs, int? expected)
    {
        Assert.Equal(expected, lhs.DivOrNull(rhs));
    }

    private class DivOrNullData : ContractTestData.Nullable<int>
    {
        public DivOrNullData()
        {
            Add(-1, -1, 1);
            Add(-1, 1, -1);
            Add(0, 1, 0);
            Add(1, -1, -1);
            Add(1, 1, 1);
            Add(4, 2, 2);

            Add(-1, 0, null);
            Add(0, 0, null);
            Add(1, 0, null);
        }
    }
}
