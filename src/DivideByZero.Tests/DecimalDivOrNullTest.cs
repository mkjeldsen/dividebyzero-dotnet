using DivideByZero;
using System;
using Xunit;

namespace DivideByZero.Tests;

public class DecimalDivOrNullTest : AbstractContractTest<decimal>
{
    [Fact]
    public override void ApiNonNullable()
    {
        decimal lhs = (rng.Next() | 1);
        decimal rhs = rng.Next();
        var expected = lhs / rhs;
        Assert.Equal(expected, lhs.DivOrNull(rhs));
    }

    [Fact]
    public override void ApiNullableLhs()
    {
        decimal? lhs = null;
        decimal? rhs = rng.Next();
        Assert.Null(lhs.DivOrNull(rhs));
    }

    [Fact]
    public override void ApiNullableRhs()
    {
        decimal? lhs = rng.Next();
        decimal? rhs = null;
        Assert.Null(lhs.DivOrNull(rhs));
    }

    [Fact]
    public override void ApiNullableLhsRhs()
    {
        decimal? lhs = null;
        decimal? rhs = null;
        Assert.Null(lhs.DivOrNull(rhs));
    }

    [Theory]
    [ClassData(typeof(DivOrNullData))]
    public override void CorrectnessReference(decimal lhs, decimal rhs, decimal? expected)
    {
        decimal? actual;
        try
        {
            actual = lhs / rhs;
        }
        catch (DivideByZeroException)
        {
            actual = null;
        }
        Assert.Equal(expected, actual);
    }

    [Theory]
    [ClassData(typeof(DivOrNullData))]
    public override void CorrectnessNonNullable(decimal lhs, decimal rhs, decimal? expected)
    {
        Assert.Equal(expected, lhs.DivOrNull(rhs));
    }

    [Theory]
    [ClassData(typeof(DivOrNullData))]
    public override void CorrectnessNullable(decimal? lhs, decimal? rhs, decimal? expected)
    {
        Assert.Equal(expected, lhs.DivOrNull(rhs));
    }

    private class DivOrNullData : ContractTestData.Nullable<decimal>
    {
        public DivOrNullData()
        {
            Add(-1, -1, 1);
            Add(-1, 1, -1);
            Add(0, 1, 0);
            Add(1, -1, -1);
            Add(1, 1, 1);
            Add(4, 2, 2);

            Add(-1, 0, null);
            Add(0, 0, null);
            Add(1, 0, null);
        }
    }
}
