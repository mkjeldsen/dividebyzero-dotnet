using DivideByZero;
using System;
using Xunit;

namespace DivideByZero.Tests;

public class IntDivOrZeroTest : AbstractContractTest<int>
{
    [Fact]
    public override void ApiNonNullable()
    {
        int lhs = (rng.Next() | 1);
        int rhs = rng.Next();
        var expected = lhs / rhs;
        Assert.Equal(expected, lhs.DivOrZero(rhs));
    }

    [Fact]
    public override void ApiNullableLhs()
    {
        int? lhs = null;
        int? rhs = rng.Next();
        Assert.Equal(0, lhs.DivOrZero(rhs));
    }

    [Fact]
    public override void ApiNullableRhs()
    {
        int? lhs = rng.Next();
        int? rhs = null;
        Assert.Equal(0, lhs.DivOrZero(rhs));
    }

    [Fact]
    public override void ApiNullableLhsRhs()
    {
        int? lhs = null;
        int? rhs = null;
        Assert.Equal(0, lhs.DivOrZero(rhs));
    }

    [Theory]
    [ClassData(typeof(DivOrZeroData))]
    public override void CorrectnessReference(int lhs, int rhs, int? expected)
    {
        int actual;
        try
        {
            actual = lhs / rhs;
        }
        catch (DivideByZeroException)
        {
            actual = 0;
        }
        Assert.Equal(expected, actual);
    }

    [Theory]
    [ClassData(typeof(DivOrZeroData))]
    public override void CorrectnessNonNullable(int lhs, int rhs, int? expected)
    {
        Assert.Equal(expected, lhs.DivOrZero(rhs));
    }

    [Theory]
    [ClassData(typeof(DivOrZeroData))]
    public override void CorrectnessNullable(int? lhs, int? rhs, int? expected)
    {
        Assert.Equal(expected, lhs.DivOrZero(rhs));
    }

    private class DivOrZeroData : ContractTestData.NonNullable<int>
    {
        public DivOrZeroData()
        {
            Add(-1, -1, 1);
            Add(-1, 1, -1);
            Add(0, 1, 0);
            Add(1, -1, -1);
            Add(1, 1, 1);
            Add(4, 2, 2);

            Add(-1, 0, 0);
            Add(0, 0, 0);
            Add(1, 0, 0);
        }
    }
}
