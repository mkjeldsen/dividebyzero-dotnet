using Xunit;

public static class ContractTestData
{
    public abstract class InputOutputData<TInput, TResult> : TheoryData<TInput, TInput, TResult>
        where TInput : struct { }

    public abstract class NonNullable<T> : InputOutputData<T, T>
        where T : struct { }

    public abstract class Nullable<T> : InputOutputData<T, T?>
        where T : struct { }
}
