# DivideByZero

`DivideByZero` is a .NET NuGet package that provides a trivial API to several
number types, via extension methods, with which one can organically specify
a fallback behavior of failing to divide by zero.

For example, instead of writing

```cs
if (b != 0)
    c = a / b;
else
    c = r;
```

you can write

```cs
c = a.DivOrZero(b);
```

and instead of writing

```cs
if (b != 0)
    c = a / b;
else
    c = a;
```

you can write

```cs
c = a.DivOrLhs(b);
```

There are a few other options, too. The main benefit of this is call site
ergonomics by avoiding noncomposable or cumbersome branching syntax.

## Should I use this?

In truth, probably not. I made this library to experiment with MSBuild
configuration and NuGet packaging. The library works as advertised and is
fleshed out and documented, but the utility is minimal and arguably not worth
involving a third party dependency for.

## License

Copyright 2023 Mikkel Kjeldsen

Licensed under the Apache License, Version 2.0 (the "License"); you may not use
this file except in compliance with the License. You may obtain a copy of the
License at <http://www.apache.org/licenses/LICENSE-2.0>

Unless required by applicable law or agreed to in writing, software distributed
under the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR
CONDITIONS OF ANY KIND, either express or implied. See the License for the
specific language governing permissions and limitations under the License.
